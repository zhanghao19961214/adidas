package edu.lanqiao.api.a;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.service.ProductServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;

@WebServlet("/api/a/yeshu")
public class Ymian extends HttpServlet{
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int pageSize=Integer.parseInt(request.getParameter("pageSize"));
		
	 JsonRequset result=null;
	 try {  
		 ProductServiceImpl sel=new ProductServiceImpl();
		Object object=sel.yemian(pageSize);
	 if(object==null) {
		 result=new JsonRequset(Constant.STATUS_SUCCESS,"查询页数成功",object);
	 }else {
		 result=new JsonRequset(Constant.STATUS_SUCCESS,"查询页数失败",object);
	 }
	 
	 }catch(Exception e) {
		 result=new JsonRequset(Constant.STATUS_FAILURE, "查询页数异常", e.getMessage());
	 }
	 finally {
		 
		 JsonRequestwriter.writer(response, result);
	 }
	 
	 
	 
	
		
	}

}
