package edu.lanqiao.api.a;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.service.ProductServiceImpl;
import edu.lanqiao.service.TalkServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;
@WebServlet("/api/a/chapl")
public class Chapl extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id= request.getParameter("product_id"); 
		
		
		JsonRequset result=null;
		 try {  
			 TalkServiceImpl talk=new TalkServiceImpl();
			Object object=talk.chapl(id);
		 if(object!=null) {
			 result=new JsonRequset(Constant.STATUS_SUCCESS,"查找成功",object);
		 }else {
			 result=new JsonRequset(Constant.STATUS_UNFOUND,"查找失败",object);
		 }
		 
		 }catch(Exception e) {
			 result=new JsonRequset(Constant.STATUS_FAILURE, "查找异常", e.getMessage());
		 }
		 finally {
			 
			 JsonRequestwriter.writer(response, result);
		 }
		
		
	}

}
