package edu.lanqiao.api.a;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.model.Talk;
import edu.lanqiao.service.ProductServiceImpl;
import edu.lanqiao.service.TalkServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;
@WebServlet("/api/a/pinglun")
public class Pinglun extends HttpServlet{
   @Override
protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 String a1= request.getParameter("TalkId");
	 String a2= request.getParameter("Content");
	 String a3= request.getParameter("Level");
	 String a4= request.getParameter("ProductId");
	 String a5= request.getParameter("Status");
	 String a6= request.getParameter("Title");
	 String a7= request.getParameter("UserId");
	   Talk talk=new Talk();
		talk.setTalkId(a1);
		talk.setTalkContent(a2);
		talk.setTalkLevel(Integer.parseInt(a3));
		talk.setTalkProductId(a4);
		talk.setTalkStatus(Integer.parseInt(a5));
		talk.setTalkTitle(a6);
		talk.setTalkUserId(a7);

		JsonRequset result=null;
		 try {  
			 TalkServiceImpl talk1=new TalkServiceImpl();
			Object object=talk1.pinglun(talk);
		 if(object!=null) {
			 result=new JsonRequset(Constant.STATUS_SUCCESS,"评论成功",object);
		 }else {
			 result=new JsonRequset(Constant.STATUS_UNFOUND,"评论失败",object);
		 }
		 
		 }catch(Exception e) {
			 result=new JsonRequset(Constant.STATUS_FAILURE, "评论异常", e.getMessage());
		 }
		 finally {
			 
			 JsonRequestwriter.writer(response, result);
		 }
}
	
}
