package edu.lanqiao.api.a;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.model.Product;
import edu.lanqiao.service.ProductServiceImpl;
import edu.lanqiao.service.TalkServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;
import edu.lanqiao.util.toString;
@WebServlet("/api/a/gxin")
public class Gxin extends HttpServlet {

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ProductServiceImpl sel=new ProductServiceImpl();
		Product product=new Product();
		product.setProductId(toString.valueOf(request.getParameter("product_id")));
	    product.setProductBuyNumber(toString.valueOf(request.getParameter("product_buy_number")));
	    product.setProductBuyScore(toString.valueOf(request.getParameter("product_buy_score")));
	    product.setProductCategoryId(toString.valueOf(request.getParameter("product_category_id")));
	    product.setProductColor(toString.valueOf(request.getParameter("product_color")));
	    product.setProductName(toString.valueOf(request.getParameter("product_name")));
	    product.setProductPhoto1(toString.valueOf(request.getParameter("product_photo1")));
	    product.setProductPhoto2(toString.valueOf(request.getParameter("product_photo2")));
	    product.setProductPhoto3(toString.valueOf(request.getParameter("product_photo3")));
	    product.setProductPhoto4(toString.valueOf(request.getParameter("product_photo4")));
	    product.setProductPhoto5(toString.valueOf(request.getParameter("product_photo5")));
	    product.setProductPrice(toString.valueOf(request.getParameter("product_price")));
	    product.setProductSeries(toString.valueOf(request.getParameter("product_series")));
	    product.setProductSex(toString.valueOf(request.getParameter("product_sex")));
	    product.setProductSize(toString.valueOf(request.getParameter("product_size")));
	    product.setProductStatus(toString.valueOf(request.getParameter("product_status")));
	    product.setProductSum(toString.valueOf(request.getParameter("product_sum")));
	    product.setProductText(toString.valueOf(request.getParameter("product_text")));
	    product.setProductUnit(toString.valueOf(request.getParameter("product_unit")));
	    product.setProductVipPrice(toString.valueOf(request.getParameter("product_vip_price")));

		
		JsonRequset result=null;
		 try {  
			 TalkServiceImpl talk1=new TalkServiceImpl();
			Object object=sel.gxin(product);
		 if(object!=null) {
			 result=new JsonRequset(Constant.STATUS_SUCCESS,"更新成功",object);
		 }else {
			 result=new JsonRequset(Constant.STATUS_UNFOUND,"更新失败",object);
		 }
		 
		 }catch(Exception e) {
			 result=new JsonRequset(Constant.STATUS_FAILURE, "更新异常", e.getMessage());
		 }
		 finally {
			 
			 JsonRequestwriter.writer(response, result);
		 }
		
	}
}
