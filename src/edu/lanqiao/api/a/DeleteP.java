package edu.lanqiao.api.a;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.service.ProductServiceImpl;
import edu.lanqiao.service.TalkServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;
@WebServlet("/api/a/deleteP")
public class DeleteP extends HttpServlet {

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id=request.getParameter("productid");
		
		JsonRequset result=null;
		 try {  
			 ProductServiceImpl sel=new ProductServiceImpl();
			Object object=sel.remove(id);
		 if(object!=null) {
			 result=new JsonRequset(Constant.STATUS_SUCCESS,"ɾ���ɹ�",object);
		 }else {
			 result=new JsonRequset(Constant.STATUS_UNFOUND,"ɾ��ʧ��",object);
		 }
		 
		 }catch(Exception e) {
			 result=new JsonRequset(Constant.STATUS_FAILURE, "ɾ���쳣", e.getMessage());
		 }
		 finally {
			 
			 JsonRequestwriter.writer(response, result);
		 }
	
	}
	
}
