package edu.lanqiao.api.l;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.service.noticeServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;

/**
 * 修改公告
 * Servlet implementation class noticeModify
 */
@WebServlet("/api/notice/modify")
public class noticeModify extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id=request.getParameter("niticeid");
		String name=request.getParameter("niticename");
		String content=request.getParameter("niticecontent");
		String time=request.getParameter("niticetime");
		String text=request.getParameter("niticetext");
		noticeServiceImpl nsi=new noticeServiceImpl();

		JsonRequset jr;
		try {
		int i=	nsi.modifyNotice(id, name, content, time, text);
		if(i==1) {
			jr=new JsonRequset(Constant.STATUS_SUCCESS,"修改成功");
		}else {
			jr=new JsonRequset(Constant.STATUS_SUCCESS,"修改失败");
		}
		}catch(Exception e) {
			jr=new JsonRequset(Constant.STATUS_SUCCESS,"修改异常");
		}
		JsonRequestwriter.writer(response,jr);
	}
}
