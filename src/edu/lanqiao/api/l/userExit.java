package edu.lanqiao.api.l;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.service.userServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;

/**
 * 用户注销
 * Servlet implementation class userExit
 */
@WebServlet("/api/user/exit")
public class userExit extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id=request.getParameter("userid");
		userServiceImpl usi=new userServiceImpl();
		JsonRequset jr;
		try {
			int i=usi.userExit(id);
			if(i==1) {
				jr=new JsonRequset(Constant.STATUS_SUCCESS,"欢迎再次使用");
			}else {
				jr=new JsonRequset(Constant.STATUS_UNFOUND,"遇到了问题");
			}
		}catch(Exception e) {
			jr=new JsonRequset(Constant.STATUS_FAILURE,"出现异常");
		}
		JsonRequestwriter.writer(response,jr);
	}

}
