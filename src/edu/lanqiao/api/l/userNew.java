package edu.lanqiao.api.l;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.model.User;
import edu.lanqiao.service.userServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;

/**
 * 查询n个月内新增用户
 * Servlet implementation class userNew
 */
@WebServlet("/api/user/new")
public class userNew extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int month=Integer.valueOf(request.getParameter("month"));
		userServiceImpl usi=new userServiceImpl();
		JsonRequset jr;
		List<User> list=new ArrayList<User>();
		try {
			list=usi.lookNew(month);
			if(list.size()>0) {
				jr=new JsonRequset(Constant.STATUS_SUCCESS,"查询成功",list);
			}else {
				jr=new JsonRequset(Constant.STATUS_UNFOUND,"查询失败");
			}
		}catch(Exception e) {
			jr=new JsonRequset(Constant.STATUS_FAILURE,"查询异常");
		}
		JsonRequestwriter.writer(response,jr);
	}
}
