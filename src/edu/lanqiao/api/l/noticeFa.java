package edu.lanqiao.api.l;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.service.noticeServiceImpl;
import edu.lanqiao.util.JsonRequset;

/**
 * 发布公告
 * Servlet implementation class noticeFa
 */
@WebServlet("/api/notice/fa")
public class noticeFa extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		noticeServiceImpl nsi=new noticeServiceImpl();
		String name=request.getParameter("name");
		String content=request.getParameter("content");
		String text=request.getParameter("text");
		JsonRequset jr;
		try {
			String s=nsi.fNotice(name, content, text);
			if(s!=null) {
				jr=new JsonRequset(Constant.STATUS_SUCCESS,"发布成功");
			}else {
				jr=new JsonRequset(Constant.STATUS_UNFOUND,"发布失败");
			}
		}catch(Exception e){
			jr=new JsonRequset(Constant.STATUS_FAILURE,"发布异常");
		}
	}
}
