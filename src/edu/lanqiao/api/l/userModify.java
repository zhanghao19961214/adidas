package edu.lanqiao.api.l;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.service.userServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;

/**
 * 修改个人信息接口
 * Servlet implementation class userModify
 */
@WebServlet("/api/user/modify")
public class userModify extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pay=request.getParameter("userpay");
		String photo=request.getParameter("userphoto");
		String password=request.getParameter("password");
		String email=request.getParameter("useremail");
		String rname=request.getParameter("rname");
		String sex=request.getParameter("sex");
		String phone=request.getParameter("usertelphone");
		String ad1=request.getParameter("useraddress1");
		String ad2=request.getParameter("useraddress2");
		String ad3=request.getParameter("useraddress3");
		String id=request.getParameter("userid");
		userServiceImpl usi=new userServiceImpl();
		JsonRequset jr;
		try {
			int i=usi.userModify(pay, password, email, rname,phone, sex,ad1, ad2, ad3, photo,id);
			if(i==1) {
				jr=new JsonRequset(Constant.STATUS_SUCCESS,"修改成功");
			}else {
				jr=new JsonRequset(Constant.STATUS_UNFOUND,"修改失败");
			}
		}catch(Exception e) {
			jr=new JsonRequset(Constant.STATUS_FAILURE,"修改异常");
		}
		JsonRequestwriter.writer(response,jr);
	}
}
