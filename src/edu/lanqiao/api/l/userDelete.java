package edu.lanqiao.api.l;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.service.userServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;

/**
 * 禁用用户接口
 * Servlet implementation class userDelete
 */
@WebServlet("/api/user/delete")
public class userDelete extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		userServiceImpl usi=new userServiceImpl();
		String id=request.getParameter("userid");
		JsonRequset jr;
		try {
			usi.userDelete(id);
			jr=new JsonRequset(Constant.STATUS_SUCCESS,"操作成功");
		}catch(Exception e) {
			jr=new JsonRequset(Constant.STATUS_SUCCESS,"操作异常");
		}
		JsonRequestwriter.writer(response,jr);
	}
}
