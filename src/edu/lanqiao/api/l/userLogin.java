package edu.lanqiao.api.l;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.model.User;
import edu.lanqiao.service.userLoginServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;
import edu.lanqiao.common.*;
/**
 * 用户登录接口
 * http://localhost:8080/adidas/api/user/login
 * Servlet implementation class userLogin
 */
@WebServlet("/api/user/login")
public class userLogin extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		userLoginServiceImpl ulsi=new userLoginServiceImpl();
		User user;
		JsonRequset jr;
		String checkcode=request.getParameter("checkcode");
		String code=(String) request.getSession().getAttribute("code");
		if(checkcode.equalsIgnoreCase(code)) {
			try {	
				user=ulsi.userLogin(username, password);
				if(user!=null) {
					jr=new JsonRequset(Constant.STATUS_SUCCESS,"登录成功",user);
				}else{
					jr=new JsonRequset(Constant.STATUS_UNFOUND,"用户名或密码错误");
				}
			}catch(Exception e) {
				jr=new JsonRequset(Constant.STATUS_FAILURE,"登录异常",e.getMessage());
			}
		}else {
			jr=new JsonRequset(Constant.STATUS_NULL,"验证码不正确");
		}
		JsonRequestwriter.writer(response,jr);
	}
	
}
