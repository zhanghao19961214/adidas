package edu.lanqiao.api.l;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.model.PageModel;
import edu.lanqiao.model.User;
import edu.lanqiao.service.userServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;

/**
 * 查询用户接口
 * Servlet implementation class userFound
 */
@WebServlet("/api/user/found")
public class userFound extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		userServiceImpl usi=new userServiceImpl();
		String pageSize=request.getParameter("pageSize");
		String pageNum=request.getParameter("pageNum");
		JsonRequset jr;
		try {
			PageModel<List> list=usi.userFound(pageSize,pageNum);
			if(list.getList().size()>0) {
				jr=new JsonRequset(Constant.STATUS_SUCCESS,"查询成功",list);
			}else {
				jr=new JsonRequset(Constant.STATUS_UNFOUND,"查询失败");
			}
		}catch(Exception e) {
			jr=new JsonRequset(Constant.STATUS_FAILURE,"查询异常");
		}
		JsonRequestwriter.writer(response,jr);
	}
}
