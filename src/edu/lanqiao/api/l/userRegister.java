package edu.lanqiao.api.l;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.model.User;
import edu.lanqiao.service.userServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;

/**
 * �û�ע��ӿ�
 * Servlet implementation class userRegister
 */
@WebServlet("/api/user/register")
public class userRegister extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		String telphone=request.getParameter("telphone");
		String email=request.getParameter("email");
		User user=new User();
		JsonRequset jr;
		user.setUserName(username);
		user.setUserPassword(password);
		user.setUserTelphone(telphone);
		user.setUserEmail(email);

		userServiceImpl ursi=new userServiceImpl();
		try {
			int i=ursi.userRegister(user);
			if(i==1) {
				jr=new JsonRequset(Constant.STATUS_SUCCESS,"ע��ɹ�",username);
			}else {
				jr=new JsonRequset(Constant.STATUS_UNFOUND,"ע��ʧ��");
			}
		}catch(Exception e) {
			jr=new JsonRequset(Constant.STATUS_FAILURE,"ע���쳣");
		}
		JsonRequestwriter.writer(response,jr);
	}
}
