package edu.lanqiao.api.l;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.service.noticeServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;

/**
 * ɾ������
 * Servlet implementation class noticeDelete
 */
@WebServlet("/api/notice/delete")
public class noticeDelete extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id=request.getParameter("noticeid");
		noticeServiceImpl usi=new noticeServiceImpl();
		JsonRequset jr;
		try {
			int i=usi.deleteNotice(id);
			if(i==1) {
				jr=new JsonRequset(Constant.STATUS_SUCCESS,"ɾ���ɹ�");
			}else {
				jr=new JsonRequset(Constant.STATUS_UNFOUND,"ɾ��ʧ��");
			}
		}catch(Exception e) {
			jr=new JsonRequset(Constant.STATUS_FAILURE,"ɾ���쳣");
		}
		JsonRequestwriter.writer(response,jr);
	}
}
