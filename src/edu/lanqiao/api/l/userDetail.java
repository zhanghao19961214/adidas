package edu.lanqiao.api.l;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.model.User;
import edu.lanqiao.service.userServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;

/**
 * ��ѯ��ϸ
 * Servlet implementation class userDetail
 */
@WebServlet("/api/notice/detail")
public class userDetail extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id=request.getParameter("userid");
		JsonRequset jr;
		userServiceImpl usi=new userServiceImpl();
		try {
			User user=usi.userDetail(id);
			if(user!=null) {
				jr=new JsonRequset(Constant.STATUS_SUCCESS,"��ѯ�ɹ�",user);
			}else {
				jr=new JsonRequset(Constant.STATUS_UNFOUND,"��ѯʧ��");
			}
		}catch(Exception e) {
			jr=new JsonRequset(Constant.STATUS_FAILURE,"��ѯ�쳣");
		}
		JsonRequestwriter.writer(response,jr);
	}
}
