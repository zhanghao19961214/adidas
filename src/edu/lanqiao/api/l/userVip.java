package edu.lanqiao.api.l;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.lanqiao.common.Constant;
import edu.lanqiao.service.userServiceImpl;
import edu.lanqiao.util.JsonRequestwriter;
import edu.lanqiao.util.JsonRequset;

/**
 * 设置vip
 * Servlet implementation class userVip
 */
@WebServlet("/api/user/vip")
public class userVip extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uid=request.getParameter("userid");
		userServiceImpl usi=new userServiceImpl();
		JsonRequset jr;
		try {
			int i=usi.makeVip(uid);
			if(i==1) {
				jr=new JsonRequset(Constant.STATUS_SUCCESS,"操作成功");
			}else {
				jr=new JsonRequset(Constant.STATUS_UNFOUND,"操作失败");
			}
		}catch(Exception e) {
			jr=new JsonRequset(Constant.STATUS_FAILURE,"操作异常");
		}
		JsonRequestwriter.writer(response,jr);
	}

}
