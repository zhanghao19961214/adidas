package edu.lanqiao.service;

import java.util.List;
import java.util.UUID;

import edu.lanqiao.dao.l.noticeDaoImpl;
import edu.lanqiao.model.Notice;
import edu.lanqiao.util.SqlHelper;

/**
 * 公告业务
 * @author lwx
 *
 */
public class noticeServiceImpl {
	/**
	 * 发布公告
	 * @return
	 */
	public String fNotice(String name,String content,String text) {
		noticeDaoImpl ndi=new noticeDaoImpl();
		String s=UUID.randomUUID().toString();
		int i=	ndi.insertNotice(s, name, content, text);
		if(i==1) {
			return s;
		}
		return null;
	}
	/**
	 * 查询
	 */
	public List<Notice> foundNotice(){
		noticeDaoImpl ndi=new noticeDaoImpl();
		return ndi.selectNotice();
	}
	/**
	 * 展示公告
	 * @return
	 */
	public List<Notice> lookNotice(String id){
		noticeDaoImpl ndi=new noticeDaoImpl();
		return ndi.selectANotice(id);
	}
	/**
	 * 删除公告
	 */
	public int deleteNotice(String id) {
		noticeDaoImpl ndi=new noticeDaoImpl();
		return ndi.deleteNotice(id);
	}
	/**
	 * 修改公告
	 */
	public int modifyNotice(String id,String name,String content,String time,String text) {
		noticeDaoImpl ndi=new noticeDaoImpl();
		return ndi.updateNotice(id, name, content, time, text);
	}
}
