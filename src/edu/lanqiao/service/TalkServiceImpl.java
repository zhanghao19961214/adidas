package edu.lanqiao.service;

import java.util.List;

import edu.lanqiao.dao.a.TalkDao;
import edu.lanqiao.model.Talk;

public class TalkServiceImpl {
	TalkDao talks=new TalkDao();
	public int pinglun(Talk talk) {
		
		return talks.insert(talk);
	}
	
	public List<Talk> chapl(String id){
		return talks.select(id);
	}
	
	public int remove(String id) {
		return talks.delete(id);
	}

}
