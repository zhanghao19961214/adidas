package edu.lanqiao.service;

import java.util.List;

import edu.lanqiao.dao.l.userDaoImpl;
import edu.lanqiao.model.Order;
import edu.lanqiao.model.PageModel;
import edu.lanqiao.model.Product;
import edu.lanqiao.model.User;
/**
 * 用户相关业务
 * @author lwx
 *
 */
public class userServiceImpl {
	/**
	 * 用户注册业务
	 * @param user
	 * @return
	 */
	public int userRegister(User user) {
		userDaoImpl udi=new userDaoImpl();
		return udi.insertUser(user);
	}
	/**
	 * 用户修改个人信息业务
	 * @param pay
	 * @param password
	 * @param email
	 * @param phone
	 * @param ad1
	 * @param ad2
	 * @param ad3
	 * @param id
	 * @return
	 */
	public int userModify(String pay,String password,String email,String rname,String phone,String sex,String ad1,String ad2,String ad3,String photo,String id) {
		userDaoImpl udi=new userDaoImpl();
		return udi.updateUser(pay, password, email, rname,phone, sex,ad1, ad2, ad3,photo,id);
	}
	/**
	 * 查询所有用户业务
	 * @return
	 */
	public PageModel<List> userFound(String pageSize,String  pageNum){
		userDaoImpl udi=new userDaoImpl();
		return udi.selectUser(pageSize,pageNum);
	}
	/**
	 * 禁用用户业务
	 */
	public void userDelete(String id) {
		userDaoImpl udi=new userDaoImpl();
		udi.deleteUser(id);
	}
	/**
	 * 查询用户最近30天的订单
	 */
	public List<Order> foundOrder(String id) {
		userDaoImpl udi=new userDaoImpl();
		return udi.selectOrder(id);
	}
	/**
	 * 查询用户订单详情
	 */
	public List<Order> foundOrderDetail(String uid,String oid) {
		userDaoImpl udi=new userDaoImpl();
		return udi.selectOrderDetail(uid,oid);
	}
	/**
	 * 根据积分查询   升 asc   降序   desc
	 */
	public List<User> userCredit(String s){
		userDaoImpl udi=new userDaoImpl();
		return udi.selectScore(s);
	}
	/**
	 * 修改用户等级
	 */
	public int makeVip(String id) {
		userDaoImpl udi=new userDaoImpl();
		return udi.updateVip(id);
	}
	/**
	 * 查询用户是否普通与会员
	 */
	public List<User> lookVip(String s){
		userDaoImpl udi=new userDaoImpl();
		return udi.selectVip(s);
	}
	/**
	 * 查询n个月内新增用户
	 */
	public List<User> lookNew(int s){
		userDaoImpl udi=new userDaoImpl();
		return udi.selectNew(s);
	}
	/**
	 * 用户注销
	 */
	public int userExit(String id) {
		userDaoImpl udi=new userDaoImpl();
		return udi.insertExit(id);
	}
	/**
	 * 查询个人信息
	 */
	public User userDetail(String id) {
		userDaoImpl udi=new userDaoImpl();
		return udi.selectUserDetail(id);
	}
	/**
	 * 多
	 * @param s1
	 * @param s2
	 * @param s3
	 * @return
	 */
	public List<Product> selectMany(String s1,String s2,String s3){
		userDaoImpl udi=new userDaoImpl();
		return udi.manySelect(s1, s2, s3);
	}
	/**
	 * 消费S
	 */
	public List<User> userXiao(){
		userDaoImpl udi=new userDaoImpl();
		return udi.selectXiao();
	}
	/**
	 * 普通
	 */
	public List<User> selectPT(){
		userDaoImpl udi=new userDaoImpl();
		return udi.selectPT();
	}
}
