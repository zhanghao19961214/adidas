package edu.lanqiao.dao.a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.lanqiao.model.Talk;
import edu.lanqiao.util.SqlHelper;

public class TalkDao {
	//插入
	public int insert(Talk talk) {
    String sql="insert INTO talk VALUES(?,?,?,?,?,?,now(),?)";
   int row= SqlHelper.update(sql, talk.getTalkId(),talk.getTalkUserId(),talk.getTalkProductId(),talk.getTalkTitle()
    		,talk.getTalkLevel(),talk.getTalkContent(),talk.getTalkStatus());
		return row;
	}
	//根据商品id 查询
	public List<Talk> select(String id){
		String sql="select * from talk WHERE talk_product_id=? ";
		List<HashMap<String, Object>> list=	SqlHelper.select3(sql, id);
		List<Talk> lists=new ArrayList<Talk>();
		if(list.size()>0&&list!=null) {
			for(int i=0;i<list.size();i++) {
			Talk talk=new Talk();
			talk.setTalkId(toString().valueOf(list.get(i).get("talk_id")));
			talk.setTalkContent(toString().valueOf(list.get(i).get("talk_content")));
			talk.setTalkLevel(Integer.parseInt(toString().valueOf(list.get(i).get("talk_level"))));
			talk.setTalkProductId(toString().valueOf(list.get(i).get("talk_product_id")));
			talk.setTalkStatus(Integer.parseInt(toString().valueOf(list.get(i).get("talk_status"))));
			talk.setTalkTitle(toString().valueOf(list.get(i).get("talk_title")));
			talk.setTalkUserId(toString().valueOf(list.get(i).get("talk_user_id")));
			lists.add(talk);
			}
		}
		return lists;
	}
	
   public int delete(String id) {
	   String sql="delete FROM talk WHERE talk_id=? ";
	   int row=SqlHelper.update(sql, id);
	   
	   return row;
   }
	

}
