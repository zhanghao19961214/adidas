package edu.lanqiao.dao.a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.lanqiao.model.Product;
import edu.lanqiao.util.Replect;
import edu.lanqiao.util.SqlHelper;
import edu.lanqiao.util.toString;
import edu.lanqiao.vo.ProductVo;

public class ProductSelect {
	/*
	 * 查找所有商品并分页  分别为 第几页 和页面展现数量
	 */
	public List<Product> select(int pageNo,int pageSize ) {
		String sql="select * FROM product ORDER BY priority DESC , product_buy_number DESC  limit ?,? ";
		List<HashMap<String, Object>> list=SqlHelper.select3(sql, pageNo,pageSize);
		List<Product> listp=new ArrayList<Product>();
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				Product product=new Product();
				product.setProductId(toString.valueOf(list.get(i).get("product_id")));
			    product.setProductBuyNumber(toString.valueOf(list.get(i).get("product_buy_number")));
			    product.setProductBuyScore(toString.valueOf(list.get(i).get("product_buy_score")));
			    product.setProductCategoryId(toString.valueOf(list.get(i).get("product_category_id")));
			    product.setProductColor(toString.valueOf(list.get(i).get("product_color")));
			    product.setProductName(toString.valueOf(list.get(i).get("product_name")));
			    product.setProductPhoto1(toString.valueOf(list.get(i).get("product_photo1")));
			    product.setProductPhoto2(toString.valueOf(list.get(i).get("product_photo2")));
			    product.setProductPhoto3(toString.valueOf(list.get(i).get("product_photo3")));
			    product.setProductPhoto4(toString.valueOf(list.get(i).get("product_photo4")));
			    product.setProductPhoto5(toString.valueOf(list.get(i).get("product_photo5")));
			    product.setProductPrice(toString.valueOf(list.get(i).get("product_price")));
			    product.setProductSeries(toString.valueOf(list.get(i).get("product_series")));
			    product.setProductSex(toString.valueOf(list.get(i).get("product_sex")));
			    product.setProductSize(toString.valueOf(list.get(i).get("product_size")));
			    product.setProductStatus(toString.valueOf(list.get(i).get("product_status")));
			    product.setProductSum(toString.valueOf(list.get(i).get("product_sum")));
			    product.setProductText(toString.valueOf(list.get(i).get("product_text")));
			    product.setProductTime(toString.valueOf(list.get(i).get("product_time")));
			    product.setProductUnit(toString.valueOf(list.get(i).get("product_unit")));
			    product.setProductVipPrice(toString.valueOf(list.get(i).get("product_vip_price")));
			    listp.add(product);
			}
		}
		return listp;
	}
	/*
	 * 总页数
	 */
	public int num(int pageSize) {
	String sql="select count(*) from product";
	List<HashMap<String, Object>> list=SqlHelper.select3(sql);
		int sum= Integer.parseInt(toString.valueOf(list.get(0).get("count(*)")));
		int n=0;
		if(sum/pageSize==0)
			n=sum/pageSize;
		else {
			n=sum/pageSize+1;
		}
		return n;
 	}
	
	public int delect(String id) {
		String sql="delete from product where product_id=?";
		int n=SqlHelper.update(sql, id);
		return n;
		
	}
	public int updata(Product product) {
		StringBuffer sql=new StringBuffer();
		sql.append("UPDATE product set product_id="+product.getProductId());
		if(product.getProductName()!=null) sql.append(",set product_name="+product.getProductName());
		if(product.getProductStatus()!=null) sql.append(",set product_status="+product.getProductStatus());
		if(product.getProductCategoryId()!=null) sql.append(",set product_category_id="+product.getProductCategoryId());
		if(product.getProductPhoto1()!=null) sql.append(",set product_photo1="+product.getProductPhoto1());
		if(product.getProductPhoto2()!=null) sql.append(",set product_photo2="+product.getProductPhoto2());
		if(product.getProductPhoto3()!=null) sql.append(",set product_photo3="+product.getProductPhoto3());
		if(product.getProductPhoto4()!=null) sql.append(",set product_photo4="+product.getProductPhoto4());
		if(product.getProductPhoto5()!=null) sql.append(",set product_photo5="+product.getProductPhoto5());
		if(product.getProductPrice()!=null) sql.append(",set product_price="+Float.parseFloat(product.getProductPrice()));
		if(product.getProductVipPrice()!=null) sql.append(",set product_vip_price="+Float.parseFloat(product.getProductVipPrice()));
		if(product.getProductSum()!=null) sql.append(",set product_sum="+ Integer.parseInt(product.getProductSum()));
		if(product.getProductUnit()!=null) sql.append(",set product_unit="+product.getProductUnit());
		if(product.getProductText()!=null) sql.append(", Set product_text="+product.getProductText());
		if(product.getProductBuyScore()!=null) sql.append(",Set product_buy_score="+Integer.parseInt(product.getProductBuyScore()));
		if(product.getProductBuyNumber()!=null) sql.append(",Set product_buy_number="+Integer.parseInt(product.getProductBuyNumber()));
		if(product.getProductSex()!=null) sql.append(",Set product_sex="+product.getProductSex());
		if(product.getProductSeries()!=null) sql.append(", Set product_series="+product.getProductSeries());
		if(product.getProductSize()!=null) sql.append(",Set product_size="+product.getProductSize());
		if(product.getProductColor()!=null) sql.append(",Set product_color="+product.getProductColor());
		sql.append("  where product_id=?");
		String sqll= sql.toString();
		int row= SqlHelper.update(sqll,product.getProductId());
		return row;
	}
	
	//模糊查询
    public List<ProductVo> selectShop(String name) {
    	String sql="select  p.product_name,p.product_photo1,p.product_price,c.category_name,t.talk_level,p.product_text\r\n" + 
    			" from product p LEFT JOIN talk t ON p.product_id=t.talk_product_id LEFT JOIN category c\r\n" + 
    			"      on p.product_category_id=c.category_id WHERE p.product_name LIKE'%?%' or p.product_text like '%?%'";
    	List<HashMap<String, Object>> list=SqlHelper.select3(sql, name,name);
    	List<ProductVo> lists=new ArrayList<ProductVo>();
    	  if(list!=null&&list.size()>0) {
    		      for(int i=0;i<list.size();i++) {
    			  ProductVo pp=new ProductVo();
    			  pp.setProduct_name(Replect.ReplaceRed(toString.valueOf(list.get(i).get("product_name")), name));
    			  pp.setCategory_name(toString.valueOf(list.get(i).get("category_name")));
    			  pp.setProduct_photo1(toString.valueOf(list.get(i).get("product_photo1")));
    			  pp.setProduct_price(toString.valueOf(list.get(i).get("product_price")));
    			  pp.setTalk_level(toString.valueOf(list.get(i).get("talk_level")));
    			  pp.setProduct_text(Replect.ReplaceRed(toString.valueOf(list.get(i).get("product_text")), name));
    			  lists.add(pp);
    		  }
    	  }
    		  
    	return lists;
    }
}
