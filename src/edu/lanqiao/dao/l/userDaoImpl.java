package edu.lanqiao.dao.l;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.lanqiao.model.Order;
import edu.lanqiao.model.PageModel;
import edu.lanqiao.model.Product;
import edu.lanqiao.model.Shopcar;
import edu.lanqiao.model.User;
import edu.lanqiao.util.SqlHelper;
import edu.lanqiao.util.toString;;
/**
 * 用户Dao
 * @author lwx
 *
 */
public class userDaoImpl {
	/**
	 * 用户注册
	 * @param user
	 * @return
	 */
	public int insertUser(User user) {
		String sql="INSERT into user VALUES(UUID(),?,null,?,'普通',NOW(),null,0,?,null,?,null,0,null,null,null,null)";
		int i=SqlHelper.update(sql,user.getUserName(),user.getUserPassword(),user.getUserEmail(),user.getUserTelphone());
		SqlHelper.close();
		return i;
	}
	/**
	 * 修改个人信息
	 * @param id
	 * @param pay
	 * @param password
	 * @param email
	 * @param phone
	 * @param ad1
	 * @param ad2
	 * @param ad3
	 * @return
	 */
	public int updateUser(String pay,String password,String email,String rname,String phone,String sex,String ad1,String ad2,String ad3,String id,String photo) {
		String sql="update user set(user_pay,user_password,user_email,user_real_name,user_telphone,user_sex,user_address1,user_address2,user_address3,user_photo) values(?,?,?,?,?,?,?,?,?,?) where user_id=?";
		int i=SqlHelper.update(sql,pay,password,email,rname,phone,sex,ad1,ad2,ad3,photo,id);
		SqlHelper.close();
		return i;
	}
	/**
	 * 查询所有用户
	 * @return
	 */
	public PageModel<List> selectUser(String pageSize,String pageNum){
		String sql="select * from user";
		PageModel pagemodel=new PageModel(sql,pageNum,pageSize);
		List<HashMap<String,Object>> list= SqlHelper.select3(pagemodel.toMysqlSql());

		List<User> l=new ArrayList<User>();	
		HashMap<String,Object> result=new HashMap<String,Object>() ;
		if(list.size()>0){
			for(int i=0;i<list.size();i++) {
				result= list.get(i);
				User user=new User();
				user.setUserId(toString.valueOf(result.get("user_id")));
				user.setUserPassword(toString.valueOf(result.get("user_password")));
				user.setUserPay(toString.valueOf(result.get("user_pay")));
				user.setUserEmail(toString.valueOf(result.get("user_email")));
				user.setUserLeavel(toString.valueOf(result.get("user_leavel")));
				user.setUserLastTime(toString.valueOf(result.get("user_last_time")));
				user.setUserRealName(toString.valueOf(result.get("user_real_name")));
				user.setUserSex(toString.valueOf(result.get("user_sex")));
				user.setUserStatus(toString.valueOf(result.get("user_status")));
				user.setUserRole(toString.valueOf(result.get("user_role")));
				user.setUserTelphone(toString.valueOf(result.get("user_telphone")));
				user.setUserAddress1(toString.valueOf(result.get("user_address1")));
				user.setUserAddress2(toString.valueOf(result.get("user_address2")));
				user.setUserAddress3(toString.valueOf(result.get("user_address3")));
				user.setUserTime(toString.valueOf(result.get("user_time")));
				user.setUserName(toString.valueOf(result.get("user_name")));
				user.setUserPhoto(toString.valueOf(result.get("user_photo")));
				l.add(user);
			}
		}
		pagemodel.setList(l);	
		List<HashMap<String,Object>> countlist= SqlHelper.select3(pagemodel.toCountSql());
		String total=toString.valueOf(countlist.get(0).get("count"));
		
		pagemodel.setTotal(Integer.valueOf(total));
		pagemodel.setTotalPage(pagemodel.getTotalPage());
		SqlHelper.close();
		
		return pagemodel;
	}
	/**
	 * 禁用用户
	 * @param id
	 */
	public void deleteUser(String id) {
		String sql="update user set user_status=1 where user_id=?";
		SqlHelper.update(sql,id);
		SqlHelper.close();
	}
	/**
	 * 查询最近30天购买
	 * @param id
	 * @return
	 */
	public List<Order> selectOrder(String id){
		String sql="SELECT * FROM `order` o LEFT OUTER JOIN shopcar s ON o.order_id = s.shopcar_order_id WHERE o.order_user_id = ? AND DATE_SUB( CURDATE( ), INTERVAL 1 MONTH ) <= date( o.order_time )";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql,id);
		HashMap<String,Object> result=new HashMap<String,Object>();
		List<Order> l=new ArrayList<Order>();
		Order order;
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				result=list.get(i);
				order=new Order();
				order.setOrderId(toString.valueOf(result.get("order_id")));
				order.setOrderContent(toString.valueOf(result.get("order_content")));
				order.setOrderIssend(toString.valueOf(result.get("order_issend")));
				order.setOrderPayway(toString.valueOf(result.get("order_payway")));
				order.setOrderVipPrice(toString.valueOf(result.get("order_vip_price")));
				order.setOrderUserId(toString.valueOf(result.get("order_user_id")));
				order.setOrderTime(toString.valueOf(result.get("order_time")));
				order.setOrderStatus(toString.valueOf(result.get("order_status")));
				order.setOrderPrice(toString.valueOf(result.get("order_price")));
				l.add(order);
			}	
		}
		SqlHelper.close();
		return l;
	}
	/**
	 * 订单详情
	 * @param uid
	 * @param oid
	 * @return
	 */
	public List<Order> selectOrderDetail(String uid,String oid) {
		String sql="SELECT * from `order` o left outer join shopcar s on o.order_id = s.shopcar_order_id left outer join product p on s.shopcar_product_id=p.product_id WHERE o.order_user_id =? and o.order_id=?";
		List<HashMap<String, Object>> list=SqlHelper.select3(sql,uid,oid);
		HashMap<String , Object> result=new HashMap<String , Object>();
		List<Order> l=new ArrayList<Order>();
		Order order;
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				order=new Order();
				result=list.get(0);
				order.setOrderId(toString.valueOf(result.get("order_id")));
				order.setOrderTime(toString.valueOf(result.get("order_time")));
				order.setOrderProductName(toString.valueOf(result.get("product_name")));
				order.setOrderProductPrice(toString.valueOf(result.get("product_price")));
				order.setOrderProductUnit(toString.valueOf(result.get("product_unit")));
				order.setOrderProductSize(toString.valueOf(result.get("product_size")));
				order.setOrderProductColor(toString.valueOf(result.get("product_color")));
				l.add(order);
			}
		}
		SqlHelper.close();
		return l;
	}
	/**
	 * 用户积分升序asc 降序desc
	 * @param s
	 * @return
	 */
	public List<User> selectScore(String s){
		String sql="select * from credit c left outer join user u on c.credit_user_id=user_id order by c.credit_score "+"  "+s;
		List<HashMap<String, Object>> list=SqlHelper.select3(sql);
		HashMap<String , Object> result=new HashMap<String , Object>();
		List<User> l=new ArrayList<User>();
		User user;
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				result= list.get(i);
				user=new User();
				user.setUserName(toString.valueOf(result.get("user_name")));
				user.setUserRealName(toString.valueOf(result.get("user_real_name")));
				user.setUserRole(toString.valueOf(result.get("user_role")));
				user.setUserSex(toString.valueOf(result.get("user_sex")));
				user.setUserStatus(toString.valueOf(result.get("user_status")));
				user.setUserTelphone(toString.valueOf(result.get("user_telphone")));
				user.setUserScore(toString.valueOf(result.get("credit_score")));
				user.setUserLeavel(toString.valueOf(result.get("user_leavel")));
				user.setUserPhoto(toString.valueOf(result.get("user_photo")));
				l.add(user);
			}
		}
		SqlHelper.close();
		return l;
	}
	/**
	 * 修改用户等级
	 * @param id
	 * @param s
	 * @return
	 */
	public int updateVip(String id) {
		String sql="update user set user_role='会员' where user_id= ? and user_role='普通'";
		int i=SqlHelper.update(sql,id);
		SqlHelper.close();
		return i;
	}
	/**
	 * 查询用户是否为普通或者会员
	 * @return
	 */
	public List<User> selectVip(String s){
		String sql="select * from user where user_role=?";
		List<HashMap<String, Object>> list=SqlHelper.select3(sql,s);
		HashMap<String , Object> result=new HashMap<String , Object>();
		List<User> l=new ArrayList<User>();
		User user;
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				result= list.get(i);
				user=new User();
				user.setUserId(toString.valueOf(result.get("user_id")));
				user.setUserName(toString.valueOf(result.get("user_name")));
				user.setUserEmail(toString.valueOf(result.get("user_email")));
				user.setUserLastTime(toString.valueOf(result.get("user_last_time")));
				user.setUserRealName(toString.valueOf(result.get("user_real_name")));
				user.setUserRole(toString.valueOf(result.get("user_role")));
				user.setUserSex(toString.valueOf(result.get("user_sex")));
				user.setUserStatus(toString.valueOf(result.get("user_status")));
				user.setUserTelphone(toString.valueOf(result.get("user_telphone")));
				user.setUserLeavel(toString.valueOf(result.get("user_leavel")));
				l.add(user);
			}
		}
		SqlHelper.close();
		return l;
	}
	/**
	 * 查询n个月内新增用户
	 * @param j
	 * @return
	 */
	public List<User> selectNew(int j){
		String sql="select * from user where DATE_SUB( CURDATE( ), INTERVAL "+j+" MONTH ) <= date(user_time )";
		List<HashMap<String, Object>> list=SqlHelper.select3(sql);
		HashMap<String , Object> result=new HashMap<String , Object>();
		List<User> l=new ArrayList<User>();
		User user;
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				result= list.get(i);
				user=new User();
				user.setUserId(toString.valueOf(result.get("user_id")));
				user.setUserName(toString.valueOf(result.get("user_name")));
				user.setUserEmail(toString.valueOf(result.get("user_email")));
				user.setUserLastTime(toString.valueOf(result.get("user_last_time")));
				user.setUserRealName(toString.valueOf(result.get("user_real_name")));
				user.setUserRole(toString.valueOf(result.get("user_role")));
				user.setUserSex(toString.valueOf(result.get("user_sex")));
				user.setUserStatus(toString.valueOf(result.get("user_status")));
				user.setUserTelphone(toString.valueOf(result.get("user_telphone")));
				user.setUserLeavel(toString.valueOf(result.get("user_leavel")));
				l.add(user);
			}
		}
		SqlHelper.close();
		return l;
	}
	/**
	 * 用户注销
	 * @param id
	 * @return
	 */
	public int insertExit(String  id) {
		String sql="insert into user user_lat_time=now() where user_id";
		int i=SqlHelper.update(sql,id);
		SqlHelper.close();
		return i;
	}
	/**
	 * 查询个人信息
	 * @param id
	 * @return
	 */
	public User selectUserDetail(String id){
		String sql="select * from credit c left outer join  user u on c.credit_user_id=u.user_id where u.user_id=?";
		List<HashMap<String,Object>> list=SqlHelper.select3(sql,id);
		User user=new User();
		HashMap<String,Object> result=new HashMap<String,Object>();
		if(list.size()>0){
				result= list.get(0);
				user.setUserId(toString.valueOf(result.get("user_id")));
				user.setUserPassword(toString.valueOf(result.get("user_password")));
				user.setUserPay(toString.valueOf(result.get("user_pay")));
				user.setUserEmail(toString.valueOf(result.get("user_email")));
				user.setUserLeavel(toString.valueOf(result.get("user_leavel")));
				user.setUserLastTime(toString.valueOf(result.get("user_last_time")));
				user.setUserRealName(toString.valueOf(result.get("user_real_name")));
				user.setUserSex(toString.valueOf(result.get("user_sex")));
				user.setUserStatus(toString.valueOf(result.get("user_status")));
				user.setUserRole(toString.valueOf(result.get("user_role")));
				user.setUserTelphone(toString.valueOf(result.get("user_telphone")));
				user.setUserAddress1(toString.valueOf(result.get("user_address1")));
				user.setUserAddress2(toString.valueOf(result.get("user_address2")));
				user.setUserAddress3(toString.valueOf(result.get("user_address3")));
				user.setUserTime(toString.valueOf(result.get("user_time")));
				user.setUserName(toString.valueOf(result.get("user_name")));
				user.setUserScore(toString.valueOf(result.get("user_score")));
				user.setUserPhoto(toString.valueOf(result.get("user_photo")));
		}
		SqlHelper.close();
		return user;
	}
	/**
	 * 多条件查询
	 * @param s1
	 * @param s2
	 * @param s3
	 * @return
	 */
	public List<Product> manySelect(String s1,String s2,String s3){
		StringBuffer sb=new StringBuffer("select * from Product p left outer join category c on p.product_category_id=c.category_id where 1=1 ");
		if(s1!=null&&!s1.equals("")){
			String sex=s1;
			sb.append("and category_son_id='"+sex+"'");
		}
		if(s2!=null&&!s2.equals("")){
			String productype=s2;
			sb.append("and category_name='"+productype+"'");
		}
		if(s3!=null&&!s3.equals("")){
			String productcate=s3;
			sb.append("and category_p_id='"+productcate+"'");
		}
		List<HashMap<String, Object>> list=SqlHelper.select3(sb.toString());
		HashMap<String , Object> result=new HashMap<String , Object>();
		List<Product> l=new ArrayList<Product>();
		Product product;
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				result= list.get(i);
				product=new Product();
				product.setProductPrice(toString.valueOf(result.get("product_price")));
				product.setProductName(toString.valueOf(result.get("product_name")));
				product.setProductColor(toString.valueOf(result.get("product_color")));
				product.setProductPhoto1(toString.valueOf(result.get("product_photo1")));
				product.setProductSex(toString.valueOf(result.get("category_son_id")));
				product.setProductPid(toString.valueOf(result.get("category_p_id")));
				product.setType(toString.valueOf(result.get("category_name")));
				l.add(product);
			}
		}
		SqlHelper.close();
		return l;
	}
	/**
	 * 查询消费过的用户
	 */
	public List<User> selectXiao(){
		String sql="select * from `order` o left outer join user u on o.order_user_id=u.user_id";
		List<HashMap<String, Object>> list=SqlHelper.select3(sql);
		HashMap<String , Object> result=new HashMap<String , Object>();
		List<User> l=new ArrayList<User>();
		User user;
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				result= list.get(i);
				user=new User();
				user.setUserId(toString.valueOf(result.get("user_id")));
				user.setUserName(toString.valueOf(result.get("user_name")));
				user.setUserEmail(toString.valueOf(result.get("user_email")));
				user.setUserLastTime(toString.valueOf(result.get("user_last_time")));
				user.setUserRealName(toString.valueOf(result.get("user_real_name")));
				user.setUserRole(toString.valueOf(result.get("user_role")));
				user.setUserSex(toString.valueOf(result.get("user_sex")));
				user.setUserStatus(toString.valueOf(result.get("user_status")));
				user.setUserTelphone(toString.valueOf(result.get("user_telphone")));
				user.setUserScore(toString.valueOf(result.get("credit_score")));
				user.setUserLeavel(toString.valueOf(result.get("user_leavel")));
				l.add(user);
			}
		}
		SqlHelper.close();
		return l;
	}
	/**
	 * 查询普通用户
	 */
	public List<User> selectPT(){
		String sql="select * from user where user_role='普通'";
		List<HashMap<String, Object>> list=SqlHelper.select3(sql);
		HashMap<String , Object> result=new HashMap<String , Object>();
		List<User> l=new ArrayList<User>();
		User user;
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				result= list.get(i);
				user=new User();
				user.setUserId(toString.valueOf(result.get("user_id")));
				user.setUserName(toString.valueOf(result.get("user_name")));
				user.setUserEmail(toString.valueOf(result.get("user_email")));
				user.setUserLastTime(toString.valueOf(result.get("user_last_time")));
				user.setUserRealName(toString.valueOf(result.get("user_real_name")));
				user.setUserRole(toString.valueOf(result.get("user_role")));
				user.setUserSex(toString.valueOf(result.get("user_sex")));
				user.setUserStatus(toString.valueOf(result.get("user_status")));
				user.setUserTelphone(toString.valueOf(result.get("user_telphone")));
				user.setUserLeavel(toString.valueOf(result.get("user_leavel")));
				l.add(user);
			}
		}
		SqlHelper.close();
		return l;
	}
}






















