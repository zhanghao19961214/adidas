package edu.lanqiao.dao.l;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import edu.lanqiao.util.*;
import edu.lanqiao.model.Notice;
import edu.lanqiao.model.Order;
import edu.lanqiao.util.SqlHelper;
/**
 * 公告表Dao
 * @author lwx
 *
 */
public class noticeDaoImpl {
	/**
	 * 发布
	 * @param name
	 * @param content
	 * @param text
	 * @return
	 */
	public int insertNotice(String s,String name,String content,String text) {
		String sql="insert into notice values(?,?,?,now(),?,0)";
		int i=SqlHelper.update(sql,s,name,content,text);
		SqlHelper.close();
		return i;
	}
	/**
	 * 查询
	 * @return
	 */
	public List<Notice> selectNotice() {
		String sql="select * from notice";
		List<HashMap<String, Object>> list=SqlHelper.select3(sql);
		HashMap<String , Object> result=new HashMap<String , Object>();
		List<Notice> l=new ArrayList<Notice>();
		Notice notice;
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				notice=new Notice();
				result=list.get(i);
				notice.setNoticeId(toString.valueOf("notice_id"));
				notice.setNoticeName(toString.valueOf("notice_name"));
				notice.setNoticeStatus(toString.valueOf("notice_status"));
				notice.setNoticeText(toString.valueOf("notice_text"));
				notice.setNoticeTime(toString.valueOf("notice_time"));
				notice.setNoticeContent(toString.valueOf("notice_content"));
				l.add(notice);
			}
		}
		SqlHelper.close();
		return l;
	}
	/**
	 * 展示 公告
	 * @param id
	 * @return
	 */
	public List<Notice> selectANotice(String id){
		String sql="select * from notice where notice_id=? and notice_status=0";
		List<HashMap<String, Object>> list=SqlHelper.select3(sql);
		HashMap<String , Object> result=new HashMap<String , Object>();
		List<Notice> l=new ArrayList<Notice>();
		Notice notice=new Notice();
		if(list.size()>0) {
				result=list.get(0);
				notice.setNoticeId(toString.valueOf("notice_id"));
				notice.setNoticeName(toString.valueOf("notice_name"));
				notice.setNoticeStatus(toString.valueOf("notice_status"));
				notice.setNoticeText(toString.valueOf("notice_text"));
				notice.setNoticeTime(toString.valueOf("notice_time"));
				notice.setNoticeContent(toString.valueOf("notice_content"));
				l.add(notice);
		}
		SqlHelper.close();
		return l;
	}
	/**
	 * 删除公告
	 * @param id
	 */
	public int deleteNotice(String id) {
		String sql="update notice set notice_status=1 where notice_id=?";
		SqlHelper.close();
		return SqlHelper.update(sql,id);
	}
	/**
	 * 修改公告
	 * @param id
	 * @param name
	 * @param content
	 * @param time
	 * @param text
	 * @return
	 */
	public int updateNotice(String id,String name,String content,String time,String text) {
		String sql="update notice set notice notice_name=?,notice_content=?,notice_time=?,notice_text=? where notice_id=?";
		int i=SqlHelper.update(sql,name,content,time,text,id);
		SqlHelper.close();
		return i;
	}
}
