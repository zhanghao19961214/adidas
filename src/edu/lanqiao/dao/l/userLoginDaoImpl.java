package edu.lanqiao.dao.l;

import java.util.HashMap;
import java.util.List;
import edu.lanqiao.util.toString;
import edu.lanqiao.model.User;
import edu.lanqiao.util.SqlHelper;
/**
 * �û���¼Dao
 * @author lwx
 *
 */
public class userLoginDaoImpl {
	public User selectUser(String username,String password) {
		User user=null;
		String sql="select * from user where user_name=? and user_password=?";
		List<HashMap<String,Object>> list= SqlHelper.select3(sql, username,password);
		if(list.size()>0) {
			HashMap<String,Object> result= list.get(0);
			user=new User();
			user.setUserId(toString.valueOf(result.get("user_id")));
			user.setUserName(toString.valueOf(result.get("user_name")));
			user.setUserPassword(toString.valueOf(result.get("user_password")));
			user.setUserEmail(toString.valueOf(result.get("user_email")));
			user.setUserLastTime(toString.valueOf(result.get("user_last_time")));
			user.setUserLeavel(toString.valueOf(result.get("user_leavel")));
			user.setUserRealName(toString.valueOf(result.get("user_real_name")));
			user.setUserRole(toString.valueOf(result.get("user_role")));
			user.setUserSex(toString.valueOf(result.get("user_sex")));
			user.setUserStatus(toString.valueOf(result.get("user_status")));
			user.setUserTelphone(toString.valueOf(result.get("user_telphone")));
			user.setUserTime(toString.valueOf(result.get("user_time")));
			user.setUserPay(toString.valueOf(result.get("user_pay")));
			user.setUserAddress1(toString.valueOf(result.get("user_address1")));
			user.setUserAddress2(toString.valueOf(result.get("user_address2")));
			user.setUserAddress3(toString.valueOf(result.get("user_address3")));
		}
		SqlHelper.close();
		return user;
	}
}
