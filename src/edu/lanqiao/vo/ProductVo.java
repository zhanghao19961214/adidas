package edu.lanqiao.vo;

public class ProductVo {
	private String product_name;
	private String product_text;
	public String getProduct_text() {
		return product_text;
	}
	public void setProduct_text(String product_text) {
		this.product_text = product_text;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public String getProduct_photo1() {
		return product_photo1;
	}
	public void setProduct_photo1(String product_photo1) {
		this.product_photo1 = product_photo1;
	}
	public String getProduct_price() {
		return product_price;
	}
	public void setProduct_price(String product_price) {
		this.product_price = product_price;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public String getTalk_level() {
		return talk_level;
	}
	public void setTalk_level(String talk_level) {
		this.talk_level = talk_level;
	}
	private String product_photo1;
	private String product_price;
	private String category_name;
	private String talk_level;

}
