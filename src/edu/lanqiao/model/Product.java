package edu.lanqiao.model;

import java.util.Date;

public class Product {
    private String productId;

    private String productName;

	private String productStatus;

	private String productCategoryId;

	private String productTime;

	private String productPhoto1;

	private String productPhoto2;

	private String productPhoto3;

	private String productPhoto4;

	private String productPhoto5;

	private String productPrice;

	private String productVipPrice;

	private String productSum;

	private String productUnit;

	private String productBuyScore;

	private String productBuyNumber;

	private String productSex;
String type;
	public String getType() {
	return type;
}

public void setType(String type) {
	this.type = type;
}

	private String productSeries;

	private String productSize;

	private String productColor;
String productPid;
	public String getProductPid() {
	return productPid;
}

public void setProductPid(String productPid) {
	this.productPid = productPid;
}

	private String productText;

	public String getProductBuyNumber() {
		return productBuyNumber;
	}

	public String getProductBuyScore() {
		return productBuyScore;
	}

	public String getProductCategoryId() {
		return productCategoryId;
	}

	public String getProductColor() {
		return productColor;
	}

	public String getProductId() {
		return productId;
	}

	public String getProductName() {
		return productName;
	}

	public String getProductPhoto1() {
		return productPhoto1;
	}

	public String getProductPhoto2() {
		return productPhoto2;
	}

	public String getProductPhoto3() {
		return productPhoto3;
	}

	public String getProductPhoto4() {
		return productPhoto4;
	}

	public String getProductPhoto5() {
		return productPhoto5;
	}

	public String getProductPrice() {
		return productPrice;
	}

	public String getProductSeries() {
		return productSeries;
	}

	public String getProductSex() {
		return productSex;
	}

	public String getProductSize() {
		return productSize;
	}

	public String getProductStatus() {
		return productStatus;
	}

	public String getProductSum() {
		return productSum;
	}

	public String getProductText() {
		return productText;
	}

	public String getProductTime() {
		return productTime;
	}

	public String getProductUnit() {
		return productUnit;
	}

	public String getProductVipPrice() {
		return productVipPrice;
	}

	public void setProductBuyNumber(String productBuyNumber) {
		this.productBuyNumber = productBuyNumber;
	}

	public void setProductBuyScore(String productBuyScore) {
		this.productBuyScore = productBuyScore;
	}

    public void setProductCategoryId(String productCategoryId) {
		this.productCategoryId = productCategoryId;
	}

    public void setProductColor(String productColor) {
		this.productColor = productColor;
	}

    public void setProductId(String productId) {
		this.productId = productId;
	}

    public void setProductName(String productName) {
		this.productName = productName;
	}

    public void setProductPhoto1(String productPhoto1) {
		this.productPhoto1 = productPhoto1;
	}

    public void setProductPhoto2(String productPhoto2) {
		this.productPhoto2 = productPhoto2;
	}

    public void setProductPhoto3(String productPhoto3) {
		this.productPhoto3 = productPhoto3;
	}

    public void setProductPhoto4(String productPhoto4) {
		this.productPhoto4 = productPhoto4;
	}

    public void setProductPhoto5(String productPhoto5) {
		this.productPhoto5 = productPhoto5;
	}

    public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}

    public void setProductSeries(String productSeries) {
		this.productSeries = productSeries;
	}

    public void setProductSex(String productSex) {
		this.productSex = productSex;
	}

    public void setProductSize(String productSize) {
		this.productSize = productSize;
	}

    public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}

    public void setProductSum(String productSum) {
		this.productSum = productSum;
	}

    public void setProductText(String productText) {
		this.productText = productText;
	}

    public void setProductTime(String productTime) {
		this.productTime = productTime;
	}

    public void setProductUnit(String productUnit) {
		this.productUnit = productUnit;
	}

    public void setProductVipPrice(String productVipPrice) {
		this.productVipPrice = productVipPrice;
	}

   
}