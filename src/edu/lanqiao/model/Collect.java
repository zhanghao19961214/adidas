package edu.lanqiao.model;

import java.util.Date;

public class Collect {
    private String collectId;

    public String getCollectId() {
		return collectId;
	}

	public void setCollectId(String collectId) {
		this.collectId = collectId;
	}

	public String getCollectUserId() {
		return collectUserId;
	}

	public void setCollectUserId(String collectUserId) {
		this.collectUserId = collectUserId;
	}

	public String getCollectCommodityId() {
		return collectCommodityId;
	}

	public void setCollectCommodityId(String collectCommodityId) {
		this.collectCommodityId = collectCommodityId;
	}

	public Date getCollectTime() {
		return collectTime;
	}

	public void setCollectTime(Date collectTime) {
		this.collectTime = collectTime;
	}

	public Integer getCollectStatus() {
		return collectStatus;
	}

	public void setCollectStatus(Integer collectStatus) {
		this.collectStatus = collectStatus;
	}

	private String collectUserId;

    private String collectCommodityId;

    private Date collectTime;

    private Integer collectStatus;

  
}