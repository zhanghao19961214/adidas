package edu.lanqiao.model;

public class Category {
    public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategorySonId() {
		return categorySonId;
	}

	public void setCategorySonId(String categorySonId) {
		this.categorySonId = categorySonId;
	}

	public String getCategoryPId() {
		return categoryPId;
	}

	public void setCategoryPId(String categoryPId) {
		this.categoryPId = categoryPId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getCategoryStatus() {
		return categoryStatus;
	}

	public void setCategoryStatus(Integer categoryStatus) {
		this.categoryStatus = categoryStatus;
	}

	private String categoryId;

    private String categorySonId;

    private String categoryPId;

    private String categoryName;

    private Integer categoryStatus;

  
}