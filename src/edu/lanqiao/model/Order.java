package edu.lanqiao.model;

import java.util.Date;

public class Order {
    private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderUserId() {
		return orderUserId;
	}

	public void setOrderUserId(String orderUserId) {
		this.orderUserId = orderUserId;
	}

	public String getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(String orderPrice) {
		this.orderPrice = orderPrice;
	}

	public String getOrderVipPrice() {
		return orderVipPrice;
	}

	public void setOrderVipPrice(String orderVipPrice) {
		this.orderVipPrice = orderVipPrice;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

	public String getOrderIssend() {
		return orderIssend;
	}

	public void setOrderIssend(String orderIssend) {
		this.orderIssend = orderIssend;
	}

	public String getOrderPayway() {
		return orderPayway;
	}

	public void setOrderPayway(String orderPayway) {
		this.orderPayway = orderPayway;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderContent() {
		return orderContent;
	}

	public void setOrderContent(String orderContent) {
		this.orderContent = orderContent;
	}

	private String orderUserId;

    private String orderPrice;

    private String orderVipPrice;

    private String orderTime;

    private String orderIssend;

    private String orderPayway;

    private String orderStatus;

    private String orderContent;

    String orderProductName;
    public String getOrderProductName() {
		return orderProductName;
	}

	public void setOrderProductName(String orderProductName) {
		this.orderProductName = orderProductName;
	}

	public String getOrderProductPrice() {
		return orderProductPrice;
	}

	public void setOrderProductPrice(String orderProductPrice) {
		this.orderProductPrice = orderProductPrice;
	}

	public String getOrderProductUnit() {
		return orderProductUnit;
	}

	public void setOrderProductUnit(String orderProductUnit) {
		this.orderProductUnit = orderProductUnit;
	}
	String orderProductColor;
	public String getOrderProductColor() {
		return orderProductColor;
	}
	public String orderProductSize;
	public String getOrderProductSize() {
		return orderProductSize;
	}

	public void setOrderProductSize(String orderProductSize) {
		this.orderProductSize = orderProductSize;
	}

	public void setOrderProductColor(String orderProductColor) {
		this.orderProductColor = orderProductColor;
	}

	String orderProductPrice;
    String orderProductUnit;
}