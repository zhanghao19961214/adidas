package edu.lanqiao.model;

public class User {
	String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getUserTime() {
		return userTime;
	}

	public void setUserTime(String userTime) {
		this.userTime = userTime;
	}

	public String getUserLastTime() {
		return userLastTime;
	}

	public void setUserLastTime(String userLastTime) {
		this.userLastTime = userLastTime;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserRealName() {
		return userRealName;
	}

	public void setUserRealName(String userRealName) {
		this.userRealName = userRealName;
	}

	public String getUserTelphone() {
		return userTelphone;
	}

	public void setUserTelphone(String userTelphone) {
		this.userTelphone = userTelphone;
	}

	public String getUserSex() {
		return userSex;
	}

	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}

	public String getUserLeavel() {
		return userLeavel;
	}

	public void setUserLeavel(String userLeavel) {
		this.userLeavel = userLeavel;
	}

	public String getUserAddress1() {
		return userAddress1;
	}

	public void setUserAddress1(String userAddress1) {
		this.userAddress1 = userAddress1;
	}

	public String getUserAddress2() {
		return userAddress2;
	}

	public void setUserAddress2(String userAddress2) {
		this.userAddress2 = userAddress2;
	}

	public String getUserAddress3() {
		return userAddress3;
	}

	public void setUserAddress3(String userAddress3) {
		this.userAddress3 = userAddress3;
	}

	String userName;

	String userPassword;

	String userRole;

	String userTime;

	String userLastTime;

	String userStatus;

	String userEmail;

	String userRealName;

	String userTelphone;

	String userSex;

	String userLeavel;

	String userAddress1;

	String userAddress2;
String userScore;
	public String getUserScore() {
	return userScore;
}

public void setUserScore(String userScore) {
	this.userScore = userScore;
}
String userPhoto;
	public String getUserPhoto() {
	return userPhoto;
}

public void setUserPhoto(String userPhoto) {
	this.userPhoto = userPhoto;
}

	String userAddress3;
String userPay;

public String getUserPay() {
	return userPay;
}

public void setUserPay(String userPay) {
	this.userPay = userPay;
}
	
}
