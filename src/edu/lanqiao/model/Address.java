package edu.lanqiao.model;

public class Address {
    private String addressId;

    public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getAddressPerson() {
		return addressPerson;
	}

	public void setAddressPerson(String addressPerson) {
		this.addressPerson = addressPerson;
	}

	public String getAddressTelphone() {
		return addressTelphone;
	}

	public void setAddressTelphone(String addressTelphone) {
		this.addressTelphone = addressTelphone;
	}

	public String getAddressAddress() {
		return addressAddress;
	}

	public void setAddressAddress(String addressAddress) {
		this.addressAddress = addressAddress;
	}

	public String getAddressStatus() {
		return addressStatus;
	}

	public void setAddressStatus(String addressStatus) {
		this.addressStatus = addressStatus;
	}

	public Integer getAddressUserId() {
		return addressUserId;
	}

	public void setAddressUserId(Integer addressUserId) {
		this.addressUserId = addressUserId;
	}

	private String addressPerson;

    private String addressTelphone;

    private String addressAddress;

    private String addressStatus;

    private  Integer  addressUserId;

   
}