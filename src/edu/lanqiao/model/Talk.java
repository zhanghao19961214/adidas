package edu.lanqiao.model;

import java.util.Date;

public class Talk {
    private String talkId;

    public String getTalkId() {
		return talkId;
	}

	public void setTalkId(String talkId) {
		this.talkId = talkId;
	}

	public String getTalkUserId() {
		return talkUserId;
	}

	public void setTalkUserId(String talkUserId) {
		this.talkUserId = talkUserId;
	}

	public String getTalkProductId() {
		return talkProductId;
	}

	public void setTalkProductId(String talkProductId) {
		this.talkProductId = talkProductId;
	}

	public String getTalkTitle() {
		return talkTitle;
	}

	public void setTalkTitle(String talkTitle) {
		this.talkTitle = talkTitle;
	}

	public Integer getTalkLevel() {
		return talkLevel;
	}

	public void setTalkLevel(Integer talkLevel) {
		this.talkLevel = talkLevel;
	}

	public Date getTalkTime() {
		return talkTime;
	}

	public void setTalkTime(Date talkTime) {
		this.talkTime = talkTime;
	}

	public Integer getTalkStatus() {
		return talkStatus;
	}

	public void setTalkStatus(Integer talkStatus) {
		this.talkStatus = talkStatus;
	}

	public String getTalkContent() {
		return talkContent;
	}

	public void setTalkContent(String talkContent) {
		this.talkContent = talkContent;
	}

	private String talkUserId;

    private String talkProductId;

    private String talkTitle;

    private Integer talkLevel;

    private Date talkTime;

    private Integer talkStatus;

    private String talkContent;

   
}