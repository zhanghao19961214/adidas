package edu.lanqiao.model;

import java.util.Date;

public class Want {
    private String wantId;

    public String getWantId() {
		return wantId;
	}

	public void setWantId(String wantId) {
		this.wantId = wantId;
	}

	public String getWantUserId() {
		return wantUserId;
	}

	public void setWantUserId(String wantUserId) {
		this.wantUserId = wantUserId;
	}

	public String getWantTitle() {
		return wantTitle;
	}

	public void setWantTitle(String wantTitle) {
		this.wantTitle = wantTitle;
	}

	public Date getWantTime() {
		return wantTime;
	}

	public void setWantTime(Date wantTime) {
		this.wantTime = wantTime;
	}

	public Date getWantLastTime() {
		return wantLastTime;
	}

	public void setWantLastTime(Date wantLastTime) {
		this.wantLastTime = wantLastTime;
	}

	public Integer getWantStatus() {
		return wantStatus;
	}

	public void setWantStatus(Integer wantStatus) {
		this.wantStatus = wantStatus;
	}

	public String getWantText() {
		return wantText;
	}

	public void setWantText(String wantText) {
		this.wantText = wantText;
	}

	private String wantUserId;

    private String wantTitle;

    private Date wantTime;

    private Date wantLastTime;

    private Integer wantStatus;

    private String wantText;

   
}