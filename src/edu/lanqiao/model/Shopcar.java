package edu.lanqiao.model;

import java.util.Date;

public class Shopcar {
    

	public String getShopcarId() {
		return shopcarId;
	}

	public void setShopcarId(String shopcarId) {
		this.shopcarId = shopcarId;
	}

	public String getShopcarOrderId() {
		return shopcarOrderId;
	}

	public void setShopcarOrderId(String shopcarOrderId) {
		this.shopcarOrderId = shopcarOrderId;
	}

	public String getShopcarProductId() {
		return shopcarProductId;
	}

	public void setShopcarProductId(String shopcarProductId) {
		this.shopcarProductId = shopcarProductId;
	}

	public String getShopcarUserId() {
		return shopcarUserId;
	}

	public void setShopcarUserId(String shopcarUserId) {
		this.shopcarUserId = shopcarUserId;
	}

	public String getShopcarProductPrice() {
		return shopcarProductPrice;
	}

	public void setShopcarProductPrice(String shopcarProductPrice) {
		this.shopcarProductPrice = shopcarProductPrice;
	}

	public String getShopcarProductCount() {
		return shopcarProductCount;
	}

	public void setShopcarProductCount(String shopcarProductCount) {
		this.shopcarProductCount = shopcarProductCount;
	}

	public String getShopcarSore() {
		return shopcarSore;
	}

	public void setShopcarSore(String shopcarSore) {
		this.shopcarSore = shopcarSore;
	}

	public String getShopcarTime() {
		return shopcarTime;
	}

	public void setShopcarTime(String shopcarTime) {
		this.shopcarTime = shopcarTime;
	}

	public String getShopcarStatus() {
		return shopcarStatus;
	}

	public void setShopcarStatus(String shopcarStatus) {
		this.shopcarStatus = shopcarStatus;
	}

	private String shopcarId;

    private String shopcarOrderId;

    private String shopcarProductId;

    private String shopcarUserId;

    private String shopcarProductPrice;

    private String shopcarProductCount;

    private String shopcarSore;

    private String shopcarTime;

    private String shopcarStatus;

  
}