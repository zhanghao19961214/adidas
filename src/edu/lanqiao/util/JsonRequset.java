package edu.lanqiao.util;

public class JsonRequset<T> {
		protected String status;
		protected String message;
		protected T  data;
		protected String error;
		
		
		public JsonRequset(String status,String message,T  data){
			this.status=status;
			this.message=message;
			this.data=data;
			
		}
		public JsonRequset(String status,String message){
			this.status=status;
			this.message=message;
			
			
		}
		
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public T getData() {
			return data;
		}
		public void setData(T data) {
			this.data = data;
		}
		
		
		
}
