package edu.lanqiao.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import edu.lanqiao.model.Product;


public class SqlHelper {
	private static Properties properties = new Properties();// 配置文件
	private static ThreadLocal<Connection> local = new ThreadLocal<Connection>();
	// 读配置文件
	static {
		try {
			properties.load(SqlHelper.class.getClassLoader().getResourceAsStream("jdbc.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static Connection conn = null;

	static {
		try {
			Class.forName(properties.getProperty("driver"));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Connection openConnection() {
		try {
			conn = DriverManager.getConnection(properties.getProperty("url"), properties.getProperty("username"),
					properties.getProperty("password"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}

	/*-----------------------------------------------------------------*/
	/*-----------------------------------------------------------------*/
	public static ResultSet select(String sql, Object... obj) {
		ResultSet rs = null;
		try {
			PreparedStatement pst = SqlHelper.openConnection().prepareStatement(sql);
			if (obj != null) {
				for (int i = 0; i < obj.length; i++) {
					pst.setObject(i + 1, obj[i]);
				}
				rs = pst.executeQuery();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("查询异常", e);
		}

		return rs;
	}

	/*-----------------------------------------------------------------*/
	/*-----------------------------------------------------------------*/
	public static <T> List<T> select2(String sql, RowHanderMapper<T> handerMapper, Object... params) {
		Connection conn = SqlHelper.openConnection();
		ResultSet rs = null;
		PreparedStatement pst = null;
		List<T> rows = null;
		try {
			pst = conn.prepareStatement(sql);
			if (params != null) {
				for (int i = 0; i < params.length; i++) {
					pst.setObject(i + 1, params[i]);
				}
				rs = pst.executeQuery();
			}
			rows = handerMapper.maping(rs);

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("查询异常", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}

		}

		return rows;
	}

	/*-----------------------------------------------------------------*/
	/*------------------------------------------------------------*/
	public static List<HashMap<String, Object>> select3(String sql, Object... params) {
		Connection conn = SqlHelper.openConnection();
		ResultSet rs = null;
		PreparedStatement pst = null;
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		try {
			
			pst = conn.prepareStatement(sql);
			if (params != null) {
				for (int i = 0; i < params.length; i++) {
					pst.setObject(i + 1, params[i]);

				}

			}
			rs = pst.executeQuery();
			ResultSetMetaData rsmate = rs.getMetaData();
			while (rs.next()) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				for (int i = 0; i < rsmate.getColumnCount(); i++) {
					String columnLabel = rsmate.getColumnLabel(i + 1);
					map.put(columnLabel, rs.getObject(columnLabel));

				}
				list.add(map);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("查询异常", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}

			}

		}

		return list;
	}

	/*-----------------------------------------------------------------*/
	/*-----------------------------------------------------------------*/
	public static int update(String sql, Object... obj) {
		int row = 0;
		PreparedStatement pst = null;
		try {
			pst = SqlHelper.openConnection().prepareStatement(sql);
			if (obj != null) {
				for (int i = 0; i < obj.length; i++) {
					pst.setObject(i + 1, obj[i]);
				}

				row = pst.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("更新异常", e);
		} finally {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return row;
	}

	public static void close() {
		Connection conn = local.get();
		if (conn != null) {
			try {
				conn.close();
				conn = null;
				local.remove();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

	/**
	 * 解析
	 */
	public static interface RowHanderMapper<T> {
		public List<T> maping(ResultSet rs);
	}

}