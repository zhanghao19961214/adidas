package edu.lanqiao.util;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class JsonRequestwriter {
		public static  void writer(HttpServletResponse response,Object object) throws IOException{
			Gson gson=new Gson();
			String json=gson.toJson(object);
			//4）响应数据的编码
			response.setCharacterEncoding("utf-8");//设置响应编码
			response.setContentType("application/json");//设置响应的数据格式MIME类型
			 
			response.getWriter().println(json);
			response.getWriter().close();
		}
}
